Widget for video implementation. You can use it like widget by js file or like a module

videoId -  ID video / link on vimeo/youtube video or id of it

youtube - true/false

vimeo - true/false

youtubeApi - if true. we can init youtube callbacks on play or stop

vimeoApi - if true. we can init vimeo callbacks on play or stop

height/width 

frameborder - borders of video

allowfullscreen: if true allow fullscreen

modal: init video in popup

allowAutoplay: after click on image of video will be start

allowControls: control panel

mouseReaction: reaction on 'click' or 'mouseover'

vimeoColor: change color of vimeo blocks

Callbacks:

beforeLoad: funciton({}) - before initialize widget

afterLoad: funciton({}) - after initialize widget

Callbacks from vimeo and youtube if API enable

videoPlay: funciton({})

videoStop: funciton({})

INSTROCTION FOR USE: 

Create block and add widget into it. Name of widget "Video Widget". Put into input area link on youtube/vimeo video.
Select the field which you need to use Youtube or Vimeo. 
For example :
Youtube : true
Vimeo : false

if 2 fields will be "true" we get exception in console on frontend