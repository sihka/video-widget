<?php

namespace Web4pro\Videowidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Posts extends Template implements BlockInterface {

    protected $_template = "Web4pro_Videowidget::widget/posts.phtml";

}