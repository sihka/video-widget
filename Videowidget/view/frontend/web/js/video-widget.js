define([
    'jquery',
    'ko',
    'Magento_Ui/js/modal/modal',
    'jquery/ui'

], function ($, ko, modal) {
    'use strict';

    $.widget("web4pro.videoEmbed", {

        options: {
            videoId: 'JqNECeH3dRU',
            youtube: false,
            vimeo: false,
            youtubeApi: false,
            vimeoApi: false,
            modal: false,
            height: 0,
            width: 0,
            frameborder: 0,
            allowAutoplay: 0,
            allowControls: 1,
            mouseReaction: 'click',
            allowfullscreen: true,
            vimeoColor: '#00adef',
            videoTrigger: 0
        },

        _create: function () {
            var self = this,
                closedOptions = {
                    head: $('head'),
                    frame: $("<iframe type='text/html'></iframe>"),
                    player: '',
                    height: 200,
                    width: 300,
                    image: $('<img class = "thumb">'),
                    div: $('<div class = "play"></div>'),
                    script: $('<script></script>'),
                    videoId: self.element.attr('data-video'),
                    modalWrapper: $('<div class = "video-widget-content"></div>'),
                    resultMouseReaction: '',
                    checkModalExist: false
                };
            this.options.videoTrigger = ko.observable(this.options.videoTrigger);

            self._trigger("beforeLoad", null);

            if(self.options.youtubeApi || self.options.modal) {

                closedOptions.script.attr("src","https://www.youtube.com/player_api");
                if (!$("script[src='https://www.youtube.com/player_api']").length) {
                    closedOptions.head.append(closedOptions.script);
                }
                self.followChanges();

            }

            if (typeof(closedOptions.videoId) !== 'undefined') {
                self.options.videoId = closedOptions.videoId;
            }

            self.checkError();

            if(self.options.youtube) {
                self.isYoutube(closedOptions);
            }

            if(self.options.vimeo) {
                self.isVimeo(closedOptions);
            }

            self._trigger("afterLoad", null);

        },

        checkError: function () {
            var self = this;

            if (self.options.youtube === false && self.options.vimeo === false) {
                throw new Error("You can't use both options (youtube or vimeo). Please use one of it. youtube: true - for example")
            }

            if (self.options.youtube === true && self.options.vimeo === true) {
                throw new Error("You could't have both options (youtube and vimeo). Please use one of it.")
            }

            if (self.options.vimeoApi === true && self.options.youtubeApi === true) {
                throw new Error("You could't use  both API (youtube and vimeo). Please use one of it.")
            }

        },

        followChanges: function () {
            var self = this;
            this.options.videoTrigger.subscribe(function (trigger) {
                if (trigger == 1) {
                    self._trigger("videoPlay", null);
                }
                if (trigger == 2) {
                    self._trigger("videoStop", null);
                }
            })
        },

        onYouTubePlayerAPIReady: function (closedOptions, self) {
                var randomId = 'player-' + self.initiateRandom() + '-' + self.options.videoId;
                    closedOptions.frame.attr('id', randomId);
                    closedOptions.frame.attr('src', 'https://www.youtube.com/embed/' + self.options.videoId +
                        "?autohide=1&controls=" + self.options.allowControls + '&showinfo=0&border=0&enablejsapi=1&origin=' + location.origin);
                    closedOptions.frame.attr('frameborder', self.options.frameborder);
                    self.checkSize(closedOptions);
                    closedOptions.frame.attr('height', closedOptions.height);
                    closedOptions.frame.attr('width', closedOptions.width);
                    self.fullScreen(self, closedOptions);

                if (!self.options.modal) {
                    self.element.empty();
                    self.element.append(closedOptions.frame);
                }
                else {
                    closedOptions.modalWrapper.append(closedOptions.frame);
                }

                closedOptions.player = new YT.Player(randomId, {
                events: {
                    'onReady': self.onPlayerReady.bind(self),
                    'onStateChange': self.onPlayerStateChange.bind(self)
                }
            });
        },

        onPlayerReady: function (event) {
            if(this.options.allowAutoplay) {
                event.target.playVideo();
            }
        },

        checkAuotuplayAllowed: function () {
            return this.options.youtube ?
                this.options.allowAutoplay ? '1&mute=1' : '0' :
                this.options.allowAutoplay ? '1&autopause=0' : '0'
        },

        onPlayerStop: function (closedOptions, self) {
            self.options.youtube ? closedOptions.player.pauseVideo() : closedOptions.player.pause();
        },

        onPlayerStateChange: function (event) {
            if(event.data === 2) {
                this.options.videoTrigger(2);
            }

            if(event.data === 1) {
                this.options.videoTrigger(1);
            }
        },

        fullScreen: function (self, closedOptions) {
            if (self.options.allowfullscreen) {
                closedOptions.frame.attr('webkitallowfullscreen', '');
                closedOptions.frame.attr('mozallowfullscreen', '');
                closedOptions.frame.attr('allowfullscreen', '');
            }
            else {
                return  false
            }
        },

        isMouseReaction: function (self) {
            var mouseReaction,
                mobile =  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

            self.options.mouseReaction === 'click' ? mouseReaction = 'click' :
                !mobile ? mouseReaction = 'mouseover' : mouseReaction = 'click';

            return mouseReaction
        },

        initiateRandom: function () {
            var self = this,
                random =  Math.floor(Math.random() * Math.floor(10000000));
            random += self.options.videoId;
            return random;
        },

        isModalActivate: function (closedOptions, frameInitialize) {
            closedOptions.modalWrapper.addClass(this.initiateRandom());
            var self = this,
                options = {
                type: 'popup',
                responsive: true,
                wrapperClass: 'video-widget-modal',
                innerScroll: true,
                title: '',
                closed: function () {
                    self.onPlayerStop(closedOptions, self);
                }
            },
            popup = modal(options, closedOptions.modalWrapper);

            if (self.options.youtube) {
                closedOptions.modalWrapper.modal('openModal');
                frameInitialize(closedOptions, self);
            }
            else {
                closedOptions.modalWrapper.modal('openModal');
            }
            closedOptions.checkModalExist = closedOptions.videoId;
        },

        validateYoutube: function (element) {
            if(element.match('v=')) {
                element = element.split('v=');
                element = element[1];
            }
            this.options.videoId = element;
        },

        validateVimeo: function (element) {
            if(element.match('com/')) {
                element = element.split('com/');
                element = element[1];
            }
            this.options.videoId = element;
        },

        isYoutube: function (closedOptions) {
            var self = this;

            self.validateYoutube(self.options.videoId);

            closedOptions.resultMouseReaction = self.isMouseReaction(self);
            closedOptions.image.attr("src", "http://i.ytimg.com/vi/" + self.options.videoId + "/maxresdefault.jpg");

            self.element.append(closedOptions.image);
            self.element.append(closedOptions.div);


            closedOptions.videoId = self.options.videoId;

            self.element.bind(closedOptions.resultMouseReaction, function () {

                if (self.options.youtubeApi || self.options.modal) {
                    if (!self.options.modal) {
                        self.onYouTubePlayerAPIReady(closedOptions, self);
                        self.element.unbind(closedOptions.resultMouseReaction);
                    }
                    else {
                        if(!closedOptions.checkModalExist) {
                            self.isModalActivate(closedOptions,  self.onYouTubePlayerAPIReady);
                        }
                        else {
                            closedOptions.modalWrapper.modal('openModal');
                        }
                    }
                }
                else {
                    self.checkSize(closedOptions);
                    self.fullScreen(self, closedOptions);
                    closedOptions.frame.attr("src", "https://www.youtube.com/embed/" + closedOptions.videoId  + "?autoplay="+ self.checkAuotuplayAllowed()
                        + "&autohide=1&controls=" + self.options.allowControls + "&showinfo=0&border=0&wmode=opaque&enablejsapi=1");
                    closedOptions.frame.attr('frameborder', self.options.frameborder);
                    closedOptions.frame.css('width', closedOptions.width);
                    closedOptions.frame.css('height', closedOptions.height);
                    self.element.empty();
                    self.element.append(closedOptions.frame);
                    self.element.unbind(closedOptions.resultMouseReaction);

                }
            })

        },

        isVimeo: function (closedOptions) {
            var self = this,
                thumbnail,
                title;

            self.validateVimeo(self.options.videoId);
            closedOptions.resultMouseReaction = self.isMouseReaction(self);
            closedOptions.videoId = self.options.videoId;
            $.ajax({
                type:'GET',
                url: 'http://vimeo.com/api/v2/video/'+ closedOptions.videoId +'.json',
                jsonp: 'callback',
                dataType: 'jsonp',
                success: function(data){
                    title = data[0].title;
                    thumbnail = data[0].thumbnail_large;
                    self.element.append('<img class = "thumb thumb-vimeo" alt="'+ title +'" src="' + thumbnail + '"/>');
                    self.element.append(closedOptions.div);
                }
            });

            this.element.bind(closedOptions.resultMouseReaction, function () {
                if (self.options.modal) {
                    if(!closedOptions.checkModalExist) {
                        self.isModalActivate(closedOptions);
                    }
                    else {
                        closedOptions.modalWrapper.modal('openModal');
                        return
                    }
                }
                self.checkSize(closedOptions);
                self.fullScreen(self, closedOptions);
                closedOptions.frame.attr("src", "https://player.vimeo.com/video/" + closedOptions.videoId +
                    "?autoplay=" + self.checkAuotuplayAllowed() + "&loop=1&title=0&byline=0&portrait=0&border=0");
                closedOptions.frame.attr('allow', 'autoplay');
                closedOptions.frame.attr('frameborder', self.options.frameborder);
                closedOptions.frame.attr('height', closedOptions.height);
                closedOptions.frame.attr('width', closedOptions.width);

                if (self.options.modal) {
                    closedOptions.modalWrapper.append(closedOptions.frame);
                }
                else {
                    self.element.empty();
                    self.element.append(closedOptions.frame);
                }

                if (self.options.vimeoApi) {

                    require(['https://player.vimeo.com/api/player.js'], function (Player) {
                        closedOptions.player = new Player(closedOptions.frame);
                        closedOptions.player.on('play', function() {
                            self._trigger("videoPlay", null);
                        });
                        closedOptions.player.on('pause', function() {
                            self._trigger("videoStop", null);
                        });
                        closedOptions.player.setColor(self.options.vimeoColor);

                    })
                }
                if (!self.options.modal) {
                    self.element.unbind(closedOptions.resultMouseReaction);
                }
            })


        },

        checkSize: function (closedOptions) {
            var self = this;

            if(self.options.height == 0 || self.options.width == 0) {

                if(self.options.modal) {
                    closedOptions.width = closedOptions.modalWrapper.outerWidth();
                    closedOptions.height = window.outerHeight / 2.5;
                }

                else {
                    closedOptions.width = self.element.outerWidth();
                    closedOptions.height = self.element.outerHeight();
                }
            }

            else {
                closedOptions.width = self.options.width;
                closedOptions.height = self.options.height;

                if(!self.options.modal) {
                    self.element.css('height',  closedOptions.height);
                    self.element.css('width',  closedOptions.width);
                }
            }
        },

    });
    return $.web4pro.videoEmbed;
});
